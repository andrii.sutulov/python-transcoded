HTTP API
========

This defines the API used to control `transcoded`

Authentication
--------------

For authentication HTTP basic auth is used with the username and password specified in the configuration file.

Submitting a job
----------------

.. code-block:: http

    POST /jobs HTTP/1.1
    Authorization: Basic somethingsomething
    Content-Type: application/json

    {
        "source": "/home/video/input.mov",
        "destination" :"/home/video/output.webm",
        "profile": "webm",
        "id": "21"
    }

You submit a job by POST'ing a json blob to the `/jobs` endpoint. The fields in the json blob:

=========== ====
Field       Description
=========== ====
source      Full path to the source file
destination Full path to the destination file
profile     The profile to use for transcoding, defined in the configuration file
id          (optional) Id for the transcoding job for reference of the sending application. This is sent back to the application with the callbacks
=========== ====

The possible responses:

====== ====
Status Body
====== ====
200    Job is queued
400    No valid json data received
400    Missing required field xxx
403    Source path not allowed for x
403    Destination path not allowed for x
404    Profile "x" is not defined
404    Source file does not exist
====== ====

Receiving callbacks
-------------------

For every jobs the requesting application will receive various callbacks at the callback url defined in the config file.
The callback is always a `POST` request.

Job started
^^^^^^^^^^^

.. code-block:: json

    {
        "status": "started",
        "source": "/home/video/input.mov",
        "destination" :"/home/video/output.webm",
        "id": "21"
    }

Job progress
^^^^^^^^^^^^

.. code-block:: json

    {
        "status": "running",
        "progress" 22.4
        "source": "/home/video/input.mov",
        "destination" :"/home/video/output.webm",
        "id": "21"
    }

Job completed
^^^^^^^^^^^^^

.. code-block:: json

    {
        "status": "done",
        "source": "/home/video/input.mov",
        "destination" :"/home/video/output.webm",
        "id": "21"
    }