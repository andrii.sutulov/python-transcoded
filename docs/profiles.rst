Transcoding profiles
====================

The profiles define the output format(s) that are acceptable for usage. With the various settings you can control how
strict it is with transcoding or just copying the streams. An example profile:

.. code-block:: ini

    [profile-webm]
    container=webm
    vcodec=vp9,vp8
    vpolicy=only-mismatch
    acodec=opus,vorbis
    apolicy=only-mismatch
    vbitrate=8M
    vbitratemax=8M
    abitrate=192k
    abitratemax=1M

This profile will create files that are compatible with the webm specifications.

container
---------

The container will define the output container format for the files, options are:

* mkv
* webm

vcodec / acodec
---------------

This defines what codec(s) are acceptable as output format. You can specify multiple codecs seperated by a comma.

If multiple codecs are specified it will always use the first codec in the list when transcoding is needed, the other
codecs in the list are used for checking if the output is acceptable.

In the example above the video codec is defined as `vp9,vp8`, in that case if the input is an H.264 stream then it will
transcode it to `vp9` since H.264 is not an acceptable format and `vp9` is the first specified format. If the input is
an `vp8` file it won't transcode the file but just copy the stream to the new container (unless the vpolicy is changed)

Supported video codecs:

* vp8
* vp9
* h264

Supported audio codecs:

* mp3
* aac
* ac3
* vorbis
* opus

vpolicy / apolicy
-----------------

The codec policy settings define what should happen if the input codec already matches the output codec options. The
default setting is `only-mismatch`, in that case it will will copy the input stream directly into the output container
without modification if the inputs match the output requirements.

The other option is `always-transcode`. In that mode it will always put the input stream through the encoder and decoder again,
even if the codec matched. This might be necessary if the input files might contain weird things that aren't supported
on your player but still use the same codec. This option is not the default since transcoding the stream is way slower
than just copying it.

vbitrate / abitrate
-------------------

This specifies the output bitrate for the video and audio stream. This is only used when the file actually gets transcoded
according the the policy specified. If this isn't specified the defaults will be used for the specific codec.

vbitratemax / abitratemax
-------------------------

This is the maximum bitrate that the input file can have without it being forced to transcode. This can be used to
override the policy defined when the input has a higher bitrate than can be played.

twopass
-------

This can be set to enable twopass encoding. The default is `false` since this usually doesn't help with the encoding
quality but is way slower.

Codec specific options
----------------------

Some codecs also have options of their own, prefixed with the codec name.