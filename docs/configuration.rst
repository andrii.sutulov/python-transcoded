Configuration
=============

The `transcoded` daemon expects an configuration file in `/etc/transcoded.ini` by default, you can specify another
configuration file by passing the `--config` parameter to the daemon.

This is an example config file:

.. code-block:: ini

    [general]
    port=12380
    listen=127.0.0.1

    [user-mediacenter]
    password=verysecret
    paths=/mnt/storage/videos
    callback=http://127.0.0.1/transcode-callback

    [profile-h264]
    container=mkv
    vcodec=h264
    vpolicy=always
    acodec=aac
    apolicy=always
    vbitrate=2M
    vbitratemax=3M
    abitrate=192k
    abitratemax=1M

Creating users
--------------

Every application that uses `transcoded` needs an user definition block in the configuration file. This block defines
the authentication information for that application and limits the application to certain paths. This is because
otherwise it would be possible to request `transcoded` to overwrite some random files that it shouldn't touch.

It also defines a callback url that is used to inform the requesting application of the transcoding progress. This
is also "hardcoded" to make sure `transcoded` isn't used to make random web requests.

The user blocks section always start with `user-` and then the username for that user. The `paths` option can contain
multiple paths by specifying a json list instead of a single value. The `paths` are used for both the input and output
paths for the files to transcode. To disable the path checking you can just specify `/` as the root path.

Creating profiles
-----------------

The profiles are an abstraction layer between your application and the transcoding backend, since ffmpeg/avconf aren't
very consistent between the various packaged versions in distro's. Since the profile definitions are the most complex
part of the configuration it is further defined in the next page