Codec specific options
======================

This is the documentation for the codec specific options in the profile definitions. Most of the info here is from the
ffmpeg wiki.

.. toctree::
   :maxdepth: 2
   :caption: Codecs:

   h264
   vp9
   vp8