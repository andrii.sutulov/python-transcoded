VP9 specific options
====================

vp9_hdr
-------
not implemented

vp9_deadline
------------
This sets the `deadline` parameter for libvpx. The default value is `good`. The options are:

======== =======
Deadline Quality
======== =======
realtime Fastest encoding, but less quality
good     Default encoding
best     Very slow encoding but better quality
======== =======

vp9_yuv420p
-----------
This forces it to translate the video to YUV420 before encoding, this is required for most video players so it is `true`
by default.

vp9_crf
-------
This sets the constant rate factor. The CRF controls the output quality. The range is 0-63

Recommended values

========== ===
Resolution CRF
========== ===
240p       37
360p       36
480p       33
720p       32
1080p      31
1440p      24
2160p      15
========== ===