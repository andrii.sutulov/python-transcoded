VP8 specific options
====================

vp8_quality
------------
This sets the `quality` parameter for libvpx. The default value is `good`. The options are:

======== =======
Quality  Description
======== =======
realtime Fastest encoding, but less quality
good     Default encoding
best     Very slow encoding but better quality
======== =======

vp8_yuv420p
-----------
This forces it to translate the video to YUV420 before encoding, this is required for most video players so it is `true`
by default.

vp8_crf
-------
This sets the constant rate factor. The CRF controls the output quality. The range is 4-63.

The default value is 10