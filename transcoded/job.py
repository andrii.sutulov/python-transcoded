import os
import shutil
import uuid
from pathlib import Path
from urllib.parse import unquote

import requests

from transcoded.executor import execute_ffmpeg
from transcoded.log import logger
from transcoded.probe import probe_file


class TranscodingJob:
    def __init__(self, source_url, callback_url, profile, send_progress_status=True):
        from .__main__ import SETTINGS
        self.send_progress_status = send_progress_status
        self.callback_url = callback_url
        self.profile = profile
        self.source_url = source_url
        self.orig_file_name = unquote(self.source_url.split('/')[-1].split('?')[0])
        self.file_name = self.build_file_name(self.orig_file_name, self.profile.get_file_extension())
        self.source_file_path = str(Path(SETTINGS['source_path'], self.file_name))
        self.target_file_path = str(Path(SETTINGS['converted_path'], self.file_name))
        self.progress_file_path = str(Path(SETTINGS['progress_path'], self.file_name))

    def build_file_name(self, orig_file_name, new_extension=None):
        file_name = '{}_{}'.format(uuid.uuid4().hex[:8], orig_file_name)
        if new_extension is not None:
            name, ext = os.path.splitext(file_name)
            file_name = '{}.{}'.format(name, new_extension)
        return file_name

    def download_file(self, url):
        logger.info('Downloading "{}" to "{}"'.format(url, self.source_file_path))
        r = requests.get(url, allow_redirects=True, stream=True)
        with open(self.source_file_path, 'wb') as f:
            f.write(r.content)

    def run(self):
        logger.info('Starting new task for source url: "{}"'.format(self.source_url))
        progress = 0
        try:
            self.download_file(self.source_url)
            probe = probe_file(self.source_file_path)
            command = self.profile.cmdline_ffmpeg(self.source_file_path, self.progress_file_path, probe)
            self._callback({
                'status': 'started',
            })
            for progress in execute_ffmpeg(command, probe):
                logger.info('Progress: {}%'.format(progress))
                if self.send_progress_status:
                    self._callback({
                        'status': 'running',
                        'progress': progress
                    })
            logger.info('Job completed')
            shutil.move(self.progress_file_path, self.target_file_path)
            os.remove(self.source_file_path)
            self._callback({
                'status': 'done',
            })
        except Exception as e:
            self._callback({
                'status': 'error',
                'progress': progress,
                'detail': str(e)
            })
            raise e

    def _callback(self, payload):
        if self.callback_url is not None:
            base = {
                'source': self.source_url,
                'profile': self.profile.name,
                'converted_file_name': self.file_name,
            }
            base.update(payload)

            try:
                response = requests.post(self.callback_url, json=base)
                if not response.ok:
                    logger.error(
                        'Callback response failed. Status code: "{}", response: {}\nrequest: {}'.format(
                            response.status_code, response.content, payload)
                    )
            except Exception as e:
                logger.error('Callback failed. Error: {}'.format(e))
