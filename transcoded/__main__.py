import configparser
import json
from pathlib import Path

import requests
from werkzeug.serving import run_simple
from werkzeug.wrappers import Request, Response
import os
import logging
import queue
import time
import shutil
from transcoded.log import logger
from transcoded.job import TranscodingJob
from transcoded.profile import Profile
import threading

werkzeug_logger = logging.getLogger('werkzeug')
werkzeug_logger.disabled = False

jobs = queue.Queue()
SETTINGS = {}

class JSONResponse(Response):
    def __init__(self, response=None, *args, **kwargs):
        response = json.dumps(response)
        kwargs['content_type'] = 'application/json'
        super().__init__(response=response, *args, **kwargs)


def jobs_index(request):
    global jobs
    console = '*/*' in request.accept_mimetypes
    if console:
        result = ""
        for job in jobs.queue:
            result += repr(job) + "\n"
        return Response(result, status=200, content_type='text/plain')
    else:
        result = json.dumps(jobs)
        return Response(result, status=200, content_type='application/json')


def job_create(request):
    global jobs
    try:
        data = json.loads(request.data.decode('utf-8'))
    except:
        msg = 'No valid json data received'
        logger.error(msg)
        return JSONResponse({'detail': msg}, status=400)

    required_fields = ['source_url', 'callback_url', 'profile']
    for field in required_fields:
        if field not in data:
            msg = 'Missing required field "{}"'.format(field)
            logger.error(msg)
            return JSONResponse({'detail': msg}, status=400)

    url_fields = ['source_url', 'callback_url']
    for url_field in url_fields:
        try:
            source_url_response = requests.head(data[url_field], allow_redirects=True)
        except Exception as e:
            msg = '"{field}" check failed: {error}'.format(field=url_field, error=e)
            logger.error(msg)
            return JSONResponse({'detail': msg}, status=400)

        if not source_url_response.ok:
            msg = '"{field}" check failed, got {code} status code.'.format(field=url_field,
                                                                           code=source_url_response.status_code)
            logger.error(msg)
            return JSONResponse({'detail': msg}, status=400)

    if data['profile'] not in profiles:
        msg = 'Profile "{}" is not defined'.format(data['profile'])
        logger.error(msg)
        return JSONResponse({'detail': msg}, status=400)

    job = TranscodingJob(source_url=data['source_url'], callback_url=data['callback_url'],
                         profile=profiles[data['profile']],
                         send_progress_status=data.get('send_progress_status', True))
    jobs.put(job)
    return JSONResponse({'detail': 'Job is queued'})


def delete_converted(request):
    global SETTINGS
    try:
        data = json.loads(request.data.decode('utf-8'))
    except:
        msg = 'No valid json data received'
        return JSONResponse({'detail': msg}, status=400)

    file_name = data.get('file_name')
    if not file_name:
        msg = '"file_name" is required.'
        logger.error(msg)
        return JSONResponse({'detail': msg}, status=400)

    path = Path(SETTINGS['converted_path'], file_name)
    if not path.exists():
        return JSONResponse({'detail': 'Not found'}, status=404)
    os.remove(path)
    return JSONResponse({}, status=204)


@Request.application
def application(request):
    try:
        if request.path == '/jobs/':
            if request.method == 'GET':
                return jobs_index(request)
            elif request.method == 'POST':
                return job_create(request)
            elif request.method == 'DELETE':
                return delete_converted(request)
    except Exception as e:
        logger.error(e)

    return JSONResponse({'detail': 'Not found'}, status=404)


def transcode_thread():
    while True:
        item = jobs.get()
        if item is None:
            time.sleep(5)
            continue

        item.run()


def parse_permissions(config):
    result = {}
    for section in config.sections():
        if section.startswith('user-'):
            username = section[5:]

            password = config.get(section, 'password', fallback=None)
            if password is None:
                logger.fatal("User '{}' has no password defined, exiting...".format(username))
                exit(1)

            raw_paths = config.get(section, 'paths', fallback='/')

            if raw_paths.strip().startswith('['):
                paths = json.loads(raw_paths)
            else:
                paths = [raw_paths]

            callback = config.get(section, 'callback', fallback=None)

            result[username] = {
                'username': username,
                'password': config.get(section, 'password'),
                'paths': paths,
                'callback': callback
            }
            logger.info('Added user {} from config'.format(username))
    return result


def parse_profiles(config):
    result = {}
    for section in config.sections():
        if section.startswith('profile-'):
            name = section[8:]
            result[name] = Profile(name, dict(config.items(section)))
            logger.info('Added profile {}'.format(result[name]))
    return result


def sanity_checks():
    if shutil.which('ffmpeg') is None and shutil.which('avconv') is None:
        print('Neither ffmpeg or avconv is installed')
        exit(1)

    if shutil.which('ffprobe') is None and shutil.which('avprobe') is None:
        print('Could not find ffprobe or avprobe')
        exit(1)


def main():
    global SETTINGS, profiles
    import argparse

    sanity_checks()

    parser = argparse.ArgumentParser(description="Transcode daemon")
    parser.add_argument('--config', '-c', help="Config file location", default='/etc/transcoded.ini', type=str)
    args = parser.parse_args()

    logger.info('Starting transcode daemon')
    if not os.path.isfile(args.config):
        logger.fatal("Config file '{}' not found".format(args.config))
        exit(1)

    config = configparser.ConfigParser()
    try:
        config.read(args.config)
    except configparser.ParsingError as e:
        logger.fatal(e.message)
        exit(1)

    SETTINGS = {
        'source_path': config.get('general', 'source_path'),
        'converted_path': config.get('general', 'converted_path'),
        'progress_path': config.get('general', 'progress_path'),
    }

    for path in SETTINGS.values():
        logger.info('creating path: "{}"'.format(path))
        Path(path).mkdir(parents=True, exist_ok=True)


    profiles = parse_profiles(config)

    logger.info('Starting transcoding thread')
    thread = threading.Thread(target=transcode_thread)
    thread.daemon = True
    thread.start()

    logger.info('Starting http server on {}:{}'.format(config.get('general', 'listen'), config.get('general', 'port')))
    run_simple(config.get('general', 'listen'), int(config.get('general', 'port')), application)


if __name__ == '__main__':
    main()
